import { TestBed, inject } from '@angular/core/testing';

import { MycourseService } from './mycourse.service';
import { MatToolbarModule } from '../../../node_modules/@angular/material';

describe('MycourseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MycourseService],
      imports: [
        MatToolbarModule
      ]
    });
  });

  it('should be created', inject([MycourseService], (service: MycourseService) => {
    expect(service).toBeTruthy();
  }));
});
