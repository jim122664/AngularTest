import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { of } from 'rxjs/observable/of';
import {CourseList} from './CourseList';

@Injectable()
export class MycourseService {
  private readCourseListUrl = '';
  // courseList: CourseList[] = [
  //   {courseId: 10000001, courseName: '測試課程1'},
  //   {courseId: 10000002, courseName: '測試課程2'}
  // ];
  courseList: CourseList[] = [];

  constructor() {
    var i;
    for (i=0;i<100;i++) {
      this.courseList.push({
        courseId: (10000001 + i),
        courseName: '測試課程' + i
      });
    }
  }

  getCourseList(): CourseList[] {
    return this.courseList;
  }
}
