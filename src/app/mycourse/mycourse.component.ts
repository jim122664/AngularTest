import { Component, OnInit } from '@angular/core';
import { CourseList } from './CourseList';
import { MycourseService } from './mycourse.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-mycourse',
  templateUrl: './mycourse.component.html',
  styleUrls: ['./mycourse.component.scss']
})
export class MycourseComponent implements OnInit {
  title = 'WM Web Develop';
  // courseList: CourseList[] = [
  //   {courseId: 10000001, courseName: '測試課程1'},
  //   {courseId: 10000002, courseName: '測試課程2'}
  // ];
  courseList: CourseList[];

  constructor(private mycourseService: MycourseService, private http: HttpClient) { }

  ngOnInit() {
    this.getCourseList();
    console.log(this.getJsonData());
  }

  getCourseList(): void {
    this.courseList = this.mycourseService.getCourseList();
  }

  select(list): void {
    console.log(list);
  }

  getJsonData() {
    return this.http.get('https://gist.githubusercontent.com/flavens/384dec1d4db0849855f5798f4141e943/raw/96fad141f38c575e872470a886949290962b9bdc/myJSONfile.json?_sort=id&_order=desc')
      .subscribe(res => {
        console.log(res);
      });
  }

}
