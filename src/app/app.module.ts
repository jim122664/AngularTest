import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MycourseComponent } from './mycourse/mycourse.component';
import {MycourseService} from './mycourse/mycourse.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatListModule, MatToolbarModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    MycourseComponent
  ],
  imports: [
    BrowserModule,
    MatButtonModule,
    MatListModule,
    HttpClientModule,
    MatToolbarModule
  ],
  providers: [MycourseService],
  bootstrap: [AppComponent],
  exports: [
    MycourseComponent
  ]
})
export class AppModule { }
