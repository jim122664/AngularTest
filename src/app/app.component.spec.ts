import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { MycourseComponent } from './mycourse/mycourse.component';
import { MycourseModule } from './mycourse/mycourse.module';
import { MatToolbarModule, MatListModule, MatButtonModule } from '../../node_modules/@angular/material';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { BrowserModule } from '../../node_modules/@angular/platform-browser';
import { MycourseService } from './mycourse/mycourse.service';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MycourseComponent
      ],
      imports: [
        MycourseModule,
        BrowserModule,
        MatButtonModule,
        MatListModule,
        HttpClientModule,
        MatToolbarModule
      ],
      providers: [
        MycourseService
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  }));
});
